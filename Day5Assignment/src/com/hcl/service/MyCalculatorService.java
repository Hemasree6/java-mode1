package com.hcl.service;

import com.hcl.exception.MyCalculatorException;
import com.hcl.model.MyCalculator;

public class MyCalculatorService extends MyCalculator {

	public long MyCalculatorService(int power, int number) throws MyCalculatorException {
		int temp = 0;
		if ((power != 0 && number != 0) && (power > 0 || number > 0)) {

			long pow = 1;
			for (int i = 1; i <= number; i++)
				pow *= power;

			temp = (int) pow;

		}

		return temp;
	}

}
