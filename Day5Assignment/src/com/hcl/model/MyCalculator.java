/*3. You are required to compute the power of a number by implementing a calculator. Create a class MyCalculator which consists of a single method long power(int, int). This method takes two integers, and , as parameters and finds . If either or is negative, then the method must throw an exception which says "". Also, if both and are zero, then the method must throw an exception which says ""
For example, -4 and -5 would result in .
Complete the function power in class MyCalculator and return the appropriate result after the power operation or an appropriate exception as detailed above.
Sample Input:
3 5
2 4
0 0
-1 -2
-1 3*/
package com.hcl.model;

public class MyCalculator {
	int number;
	int power;

	public MyCalculator() {
		super();
	}

	public MyCalculator(int number, int power) {
		super();
		this.number = number;
		this.power = power;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}

}
