/*5. Given total runs scored and total overs faced as the input. Write a program to calculate the run rate with the formula,
Run rate= total runs scored/total overs faced.
Use BufferedReader class to get the inputs from user.
This program may generate Arithmetic Exception / NumberFormatException. Use exception handling mechanisms to handle this exception. Use a single catch block. In the catch block, print the class name of the exception thrown.
Sample Input and Output 1:
Enter the total runs scored
79
Enter the total overs faced
14
Current Run Rate : 5.64*/
package com.hcl.model;

public class RunRate {
	private int totalRuns;
	private int totalOvers;

	public RunRate() {
		super();
	}

	public RunRate(int totalRuns, int totalOvers) {
		super();
		this.totalRuns = totalRuns;
		this.totalOvers = totalOvers;
	}

	public int getTotalRuns() {
		return totalRuns;
	}

	public void setTotalRuns(int totalRuns) {
		this.totalRuns = totalRuns;
	}

	public int getTotalOvers() {
		return totalOvers;
	}

	public void setTotalOvers(int totalOvers) {
		this.totalOvers = totalOvers;
	}

}
