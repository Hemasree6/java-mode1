package com.hcl.exception;

public class MyCalculatorException extends Exception {

	public String zero(int pow, int num) {
		String err = null;
		if (pow == 0 && num == 0)
			err = "java.lang.Exception: n and p should not be zero.";
		return err;
	}

	public String negative(int pow, int num) {
		String err = null;
		if (pow < 0 || num < 0)
			err = "java.lang.Exception: n and p should not be negative.";
		return err;
	}

	@Override
	public String getMessage() {

		return "java.lang.Exception: n and p should not be negative."
				+ "java.lang.Exception: n and p should not be zero.";
	}

}
