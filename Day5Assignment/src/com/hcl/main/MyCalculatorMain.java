package com.hcl.main;

import java.util.Scanner;

import com.hcl.exception.MyCalculatorException;

import com.hcl.service.MyCalculatorService;

public class MyCalculatorMain {

	public static void main(String[] args) throws MyCalculatorException {
		Scanner scanner = new Scanner(System.in);
		MyCalculatorService calculatorService = new MyCalculatorService();
		MyCalculatorException exception = new MyCalculatorException();
		try {
			System.out.println("Enter the power and number");
			int pow = scanner.nextInt();
			int num = scanner.nextInt();

			if ((pow != 0 && num != 0) && (num > 0 && pow > 0)) {
				System.out.println(pow + " power of " + num + " is " + calculatorService.MyCalculatorService(pow, num));

			} else if (pow == 0 && num == 0) {
				System.out.println("Exception" + exception.zero(pow, num));
			} else if (pow < 0 || num < 0) {
				System.out.println("Exception" + exception.negative(pow, num));
			}

		} catch (MyCalculatorException e) {
			System.out.println(e.getMessage());

		} finally {
			scanner = null;
			calculatorService = null;
		}
	}

}
