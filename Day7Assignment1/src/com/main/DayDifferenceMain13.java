package com.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import com.model.DayDifference13;

public class DayDifferenceMain13 {
	public static void main(String[] args) throws IOException, ParseException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter first date");
		String s1 = br.readLine();
		System.out.println("Enter second date");
		String s2 = br.readLine();
		System.out.println(DayDifference13.getDateDifference(s1, s2));

	}

}
