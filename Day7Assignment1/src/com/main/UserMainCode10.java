/*10.Write a program to read  two String variables in DD-MM-YYYY.Compare the two dates and return the older date in 'MM/DD/YYYY' format. s
Include a class UserMainCode with a static method findOldDate which accepts the string values. The return type is the string. 
Create a Class Main which would be used to accept the two string values and call the static method present in UserMainCode.
Sample Input 1: 
05-12-1987 
8-11-2010 
Sample Output 1: 
12/05/1987 */
package com.main;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class UserMainCode10 {

	public static void main(String[] args) throws ParseException {
		SimpleDateFormat sdformat = new SimpleDateFormat("yyyy-MM-dd");
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter first date in formate 'yyy-mm-dd'");
		String d1 = scanner.next();
		System.out.println("Enter second date in formate 'yyy-mm-dd'");
		String d2 = scanner.next();
		Date d3 = sdformat.parse(d1);
		Date d4 = sdformat.parse(d2);
		System.out.println("The date 1 is: " + sdformat.format(d3));
		System.out.println("The date 2 is: " + sdformat.format(d4));
		if (d3.compareTo(d4) > 0) {
			System.out.println("Date 1 occurs after Date 2:" + d2);
		} else if (d3.compareTo(d4) < 0) {
			System.out.println("Date 1 occurs before Date 2:" + d1);
		} else if (d3.compareTo(d4) == 0) {
			System.out.println("Both dates are equal");
		}

	}
}
