package com.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;

import com.model.UserModelCode11;

public class UserMainCode11 {
	public static void main(String[] args) throws IOException, ParseException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the date in yyyy-mm-dd");
		String s1 = br.readLine();
		String s2 = br.readLine();

		System.out.println(UserModelCode11.getMonthDifference(s1, s2));
	}

}
