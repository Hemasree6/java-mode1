package com.main;

import java.util.Scanner;

import com.model.UserModelCode12;

public class UserMainCode12 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Ip Address:");

		String ipAddress = sc.nextLine();

		boolean b = UserModelCode12.ipValidator(ipAddress);
		if (b == true)
			System.out.println("Valid");
		else
			System.out.println("Invalid");
	}

}
