package com.main;

import java.util.Scanner;

import com.model.UserModelCode6;

public class UserMainCode6 {

	public static void main(String[] args) {
		int year, month;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the year :");
		year = scanner.nextInt();
		System.out.println("Enter the month in formate where january=0 and December=11 :");
		month = scanner.nextInt();
		System.out.println("Number of days in " + month + " of " + year + " is :");
		System.out.println(UserModelCode6.getNumberofDays(year, month));
		scanner.close();

	}

}
