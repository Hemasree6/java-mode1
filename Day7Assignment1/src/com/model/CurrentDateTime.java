//1.Write a java program to print current date and time in the specified format. 

package com.model;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;    

public class CurrentDateTime {
	 public static void main(String[] args) {    
		   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
		   LocalDateTime now = LocalDateTime.now();  
		   System.out.println( "Current Date and Time is : " + dtf.format(now));  
	}

}
