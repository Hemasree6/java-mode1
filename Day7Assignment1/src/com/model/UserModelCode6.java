/*6.Include a class UserMainCode with a static method �getNumberOfDays� that accepts 2 integers as arguments and returns an integer. The first argument corresponds to the year and the second argument corresponds to the month code. The method returns an integer corresponding to the number of days in the month. 
Create a class Main which would get 2 integers as input and call the static method getNumberOfDays present in the UserMainCode. 
Input and Output Format: 
Input consists of 2 integers that correspond to the year and month code. 
Output consists of an integer that correspond to the number of days in the month in the given year. 
Sample Input: 
2000 
1 
Sample Output: 
29 */
package com.model;

public class UserModelCode6 {

	public static int getNumberofDays(int year, int month) {
		if (month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11)
			return 31;
		else if ((month == 1) && ((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0))) {
			return 29;
		} else if (month == 1) {
			return 28;
		} else
			return 30;
	}

}
