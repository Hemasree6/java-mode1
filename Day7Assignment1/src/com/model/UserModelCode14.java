/*14.Given a date string in the format dd/mm/yyyy, write a program to convert the given date to the format dd-mm-yy. 
Include a classUserMainCode with a static method “convertDateFormat” that accepts a String and returns a String. 
Create a class Main which would get a String as input and call the static method convertDateFormat present in the UserMainCode. 
Sample Input: 
12/11/1998 
Sample Output: 
12-11-98 */
package com.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserModelCode14 {

	public static Date convertDateFormate(String str) throws ParseException {

		SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");

		Date date = formatter.parse(str);

		return date;
	}
}
