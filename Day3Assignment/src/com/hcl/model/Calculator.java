/*10. I have created a class Calculator inside a package name com.hcl
package com.hcl;
public class Calculator {
public int add(int a, int b){
return a+b;
}
}
how to use add method from another package.*/
package com.hcl.model;

public class Calculator {
	public int add(int a, int b) {
		return a + b;
	}
}
