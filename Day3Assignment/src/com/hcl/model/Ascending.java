//3. write a Java program to sort an integer array of 10 elements in ascending.
package com.hcl.model;

public class Ascending {
	public static void printArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i] + " ");
		}
		System.out.println();
	}

	public static void sortArray(int[] array) {
		int temp = 0;
		for (int i = 0; i < array.length; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (array[i] > array[j]) {
					temp = array[i];
					array[i] = array[j];
					array[j] = temp;
				}

			}
		}
		System.out.println("Elements of array sorted in Ascending order : ");
		printArray(array);

	}
}
