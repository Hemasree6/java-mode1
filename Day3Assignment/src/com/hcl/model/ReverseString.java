/*9. Write a program to read a string and a character, and reverse the string and convert it in a format such that each character is separated by the given character. Print the final string.
Sample Input:
Rabbit
Sample Output:
t-i-b-b-a-R*/
package com.hcl.model;

public class ReverseString {

	public String wordReverse(String str) {
		String reverse = "";
		for (int i = str.length(); i > 0; i--) {
			reverse = reverse + (str.charAt(i - 1));

		}
		return reverse;
	}
}
