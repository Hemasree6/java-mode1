//4. write a Java program to search for an element of an integer array of 10 elements.
package com.hcl.model;

public class Search {
	public static int linearsearch(int[] array, int key) {
		int value = 0;
		for (int i = 0; i < array.length; i++) {
			if (array[i] == key) {
				value = i;
			}
		}
		return value;
	}

}
