//2. Write a Java program to replace all the 'd' occurrence characters with �h� characters in each string.
package com.hcl.model;

public class ReplaceChar {
	public String replaceChar(String str) {
		String string = str.replace('d', 'h');
		System.out.println("Original string:" + str);
		return string;
	}
}
