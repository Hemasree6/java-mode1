/*6. Given a string , print �Yes� if it is a palindrome, print �No� otherwise.
Sample Input
madam
Sample Output
Yes*/
package com.hcl.model;

public class Palindrome {
	public String checkPalindrome(String str) {
		String reverse = "";
		for (int i = str.length() - 1; i >= 0; i--) {
			reverse = reverse + str.charAt(i);

		}
		return reverse;
	}

}
