//1. Write a Java program to convert all the characters in a string to lowercase.

package com.hcl.model;

public class LowerCase {
	public static void main(String[] args) {
		String str = "Hello world";
		String lowerstring = str.toLowerCase();
		System.out.println("Original string : " + str);
		System.out.println("String in lowercase :  " + lowerstring);
	}

}
