package com.hcl.model;

/*8. Write a program to read a string and return a modified string based on the following rules.
Return the String without the first 2 characters except when
a. Keep the first char if it is 'k'
b. Keep the second char if it is 'b'.
Sample Input 1:
hello
Sample Output 1:
llo
Sample Input 2:
kava
Sample Output 2:
kva*/

public class StringRule {
	public String remove(String str) {
		if (str.charAt(0) == 'k') {
			str = str.substring(2, str.length());
			return 'k' + str;
		} else if (str.charAt(1) == 'b') {
			str = str.substring(1, str.length());
		} else {
			str = str.substring(2, str.length());
		}
		return str;

	}

}
