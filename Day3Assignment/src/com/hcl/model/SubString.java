/*5.Accept a string, and two indices(integers), and print the substring consisting of all characters inclusive range from ..to .
Sample Input
Helloworld
3 7
Sample Output
Lowo*/
package com.hcl.model;

public class SubString {
	public String subString(String string, int start, int end) {
		String temp = string.substring(start, end);
		return temp;
	}
}
