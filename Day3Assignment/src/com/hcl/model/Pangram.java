/*7. Write a program to find the given string is pangram or not.
 (Hint: Pangrams are words or sentences containing every letter of the alphabet at least once)
Sample Input
The quick brown fox jumps over the lazy dog sp.
Sample Output:
Pangram
 */
package com.hcl.model;

public class Pangram {
	public static void main(String[] args) {
		String str = "The quick brown for jumps over the lazy dog";
		boolean[] alphalist = new boolean[26];
		int index = 0;
		int flag = 1;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
				index = str.charAt(i) - 'A';

			} else if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z')
				index = str.charAt(i) - 'a';
		}
		alphalist[index] = true;
		for (int i = 0; i <= 25; i++) {
			if (alphalist[index] == false) {
				flag = 0;
			}
		}
		System.out.println("String : " + str);
		if (flag == 1) {
			System.out.println("The given string is pangram");
		} else {
			System.out.println("The given string is pangram");

		}
	}
}
