package com.hcl.main;

import java.util.Scanner;
import com.hcl.model.ReverseString;

public class ReverseStringMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		ReverseString reverseString = new ReverseString();
		System.out.println("Enter a string :  ");
		String str = scanner.next();
		System.out.println(" reversed string is : " + reverseString.wordReverse(str));
		scanner.close();

	}
}
