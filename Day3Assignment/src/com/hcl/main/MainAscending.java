package com.hcl.main;

import com.hcl.model.Ascending;

public class MainAscending {
	public static void main(String[] args) {
		int[] obj = new int[] { 6, -5, 9, 1, 3, 8 };
		System.out.println("Elements of given Array : ");
		Ascending.printArray(obj);
		Ascending.sortArray(obj);

	}

}
