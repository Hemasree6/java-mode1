package com.hcl.main;

import java.util.Scanner;

import com.hcl.model.StringRule;

public class StringRuleMain {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		StringRule stringRule = new StringRule();
		System.out.println("Enter a string:");
		String temp = scanner.next();
		System.out.println(" String after checking String Rule : " + stringRule.remove(temp));
		scanner.close();
	}
}