package com.hcl.pp.service;

import com.hcl.pp.dao.RegistrationDao;
import com.hcl.pp.model.User;

public class UserValidator {
	public boolean validate(User user, String confirmPassword){
		RegistrationDao registrationDao=new RegistrationDao();
		if(registrationDao.read(user)){
			return false;
		}
		else if(!user.getPassword().equals(confirmPassword)){
			return false;
		}
		else{
			registrationDao	.create(user);
			return true;
		}
	}
}
