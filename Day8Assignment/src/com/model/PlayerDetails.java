/*6. Write a java program to record the player details into the file. Get the player details name, teamName and number of matches played from the user and write those information in a comma seperated format (CSV). 
Below is the format of the output file. 
<name>,<teamName>,<numberOfMatches> 
eg: Virat Kohli,Royal Challengers Bangalore,16 
Create a main class "Main.java" 
Using File class create a new file(player.csv) and write using the OutputStream. 
Sample Input/Output : 
Enter the name of the player 
Virat Kohli 
Enter the team name 
Royal Challengers Banglore 
Enter the number of matches played 
16 */
package com.model;

public class PlayerDetails {

	String name;
	String teamName;
	int matches;

	public PlayerDetails() {
		super();
	}

	public PlayerDetails(String name, String teamName, int matches) {
		super();
		this.name = name;
		this.teamName = teamName;
		this.matches = matches;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public int getMatches() {
		return matches;
	}

	public void setMatches(int matches) {
		this.matches = matches;
	}

}
