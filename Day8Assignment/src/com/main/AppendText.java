//3. Write a Java program to append text to an existing file. 
package com.main;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;

public class AppendText {

	public static void main(String[] args) throws Exception {
		FileWriter fw = null;
		BufferedWriter bw = null;
		PrintWriter pw = null;

		try {
			fw = new FileWriter("G:\\9th feb to 29th mar\\File\\WriteRead.txt", true);
			bw = new BufferedWriter(fw);
			pw = new PrintWriter(bw);

			pw.println("Welcome");
			pw.println("To");
			pw.println("JAVA PROGRAMMING");
			System.out.println("Data Successfully appended into file");
			pw.flush();
		} finally {

			pw.close();
			bw.close();
			fw.close();
		}
	}

}
