//5. Write a Java program to find the longest word in a text file. 

package com.main;

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class LongestWordInFile {

	public static void main(String[] args) throws FileNotFoundException {
		new LongestWordInFile().findLongestWords();

	}

	public String findLongestWords() throws FileNotFoundException {
		String longest_word = " ";
		String current;
		Scanner sc = new Scanner(new File("G:\\9th feb to 29th mar\\File\\WriteRead.txt"));

		while (sc.hasNext()) {
			current = sc.next();
			if (current.length() > longest_word.length()) {
				longest_word = current;

			}
		}

		System.out.println(longest_word);
		return longest_word;
	}
}
