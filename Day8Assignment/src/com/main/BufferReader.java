/*1. Write a Java program by using BufferedReader class to prompt a user to input his/her name and then the output will be shown as an example below: 
Hello HCL!  */

package com.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class BufferReader {
	public static void main(String[] args) {
		InputStreamReader r = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(r);
		System.out.println("Enter your name");
		String name = null;
		try {
			name = br.readLine();
		} catch (IOException e) {

			e.printStackTrace();
		}
		System.out.println("Hello " + name + "!");
	}

}
