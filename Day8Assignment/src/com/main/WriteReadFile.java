//2. Write a Java program to write and read a plain text file. 
package com.main;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class WriteReadFile {

	public static void main(String[] args) {
		String data = "Data to be written on a file";
		char[] array = new char[100];
		try {
			FileWriter output = new FileWriter("G:\\9th feb to 29th mar\\File\\WriteRead.txt");
			output.write(data);
			System.out.println("Success");
			output.close();
			FileReader input = new FileReader("G:\\9th feb to 29th mar\\File\\WriteRead.txt");
			input.read(array);
			System.out.println(array);

		} catch (IOException e) {

			e.printStackTrace();
		}

	}
}
