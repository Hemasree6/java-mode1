package com.hcl.main;

import com.hcl.model.A;

public class OOPExercises4 {

	public static void main(String[] args) {

		A objA = new A(); // import classA

		System.out.println("in main(): ");

		System.out.println("objA.a = " + objA.a);// change variable to public

		objA.a = 222;

	}
}
