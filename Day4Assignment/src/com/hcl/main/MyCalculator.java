package com.hcl.main;

import java.util.Scanner;

import com.hcl.model.AdvancedArithmetic;

class MyCalculator implements AdvancedArithmetic {

	@Override
	public int divisorSum(int n) {

		int j = 0;

		for (int i = 1; i <= n; i++) {
			if (n % i == 0) {
				j = j + i;
			}

		}
		return j;

	}

	public static void main(String[] args) {
		MyCalculator calculator = new MyCalculator();
		Scanner scanner = new Scanner(System.in);
		try {
			System.out.println("Enter the number:");
			int number = scanner.nextInt();
			System.out.println("I implemented:AdvancedArithmetic");
			System.out.println("The Sum Of The Divisors is: ");

			System.out.println(calculator.divisorSum(number));
			scanner.close();
		} finally {
			scanner = null;
			calculator = null;

		}
	}
}
