package com.hcl.main;

import com.hcl.model.A6;
import com.hcl.model.B6;
import com.hcl.model.C6;

public class OOPExercises6 {
	static int a = 555;

	public static void main(String[] args) {

		A6 objA = new A6();

		B6 objB1 = new B6();

		A6 objB2 = new B6();

		C6 objC1 = new C6();

		B6 objC2 = new C6();

		A6 objC3 = new C6();

		objA.display();

		objB1.display();

		objB2.display();

		objC1.display();

		objC2.display();

		objC3.display();
	}

}
