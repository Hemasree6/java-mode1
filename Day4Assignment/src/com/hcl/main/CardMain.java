package com.hcl.main;

import java.util.Scanner;

import com.hcl.model.Card;
import com.hcl.model.MembershipCard;
import com.hcl.model.PaybackCard;

public class CardMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Card card;
		PaybackCard paybackCard = new PaybackCard();
		MembershipCard membershipCard = new MembershipCard();
		int option = 0;
		String cardNo = null;
		String name = null;
		String expDate = null;
		int pointsearned = 0;
		double totalAmt = 0;
		int rating = 0;

		try {
			System.out.println("Select the Card");
			System.out.println("1.Payback Card");
			System.out.println("2.Membership Card");
			option = scanner.nextInt();

			switch (option) {
			case 1:
				System.out.println("Enter the card details -Holder name,Card no,ExpiryDate");

				name = scanner.next();
				paybackCard.setHolderName(name);
				cardNo = scanner.next();
				paybackCard.setCardNumber(cardNo);
				expDate = scanner.next();
				paybackCard.setExpiryDate(expDate);
				System.out.println("Enter points in number");
				pointsearned = scanner.nextInt();
				paybackCard.setPointsEarned(pointsearned);
				System.out.println("enter amount");
				totalAmt = scanner.nextDouble();
				paybackCard.setTotalAmount(totalAmt);
				System.out.println(name + "'s" + "Payback Card details:");
				System.out.println("");
				System.out.println("Holder Name:" + paybackCard.getHolderName());
				System.out.println("Card Number:" + paybackCard.getCardNumber());
				System.out.println("Points Earned:" + paybackCard.getPointsEarned());
				System.out.println("Total Amount:" + paybackCard.getTotalAmount());

				break;
			case 2:
				System.out.println("Enter the card details -Holder name,Card no,ExpiryDate");

				name = scanner.next();
				membershipCard.setHolderName(name);
				cardNo = scanner.next();
				membershipCard.setCardNumber(cardNo);
				expDate = scanner.next();
				membershipCard.setExpiryDate(expDate);
				System.out.println("Enter rating in number");
				rating = scanner.nextInt();
				membershipCard.setRating(rating);
				System.out.println(name + "'s" + "MemberShip Card details:");
				System.out.println("");
				System.out.println("Holder Name:" + membershipCard.getHolderName());
				System.out.println("Card Number:" + membershipCard.getCardNumber());
				System.out.println("Rating entered:" + membershipCard.getRating());

			default:

				System.err.println("Select valid option");

				break;

			}

		} finally {
			scanner = null;
			paybackCard = null;
			membershipCard = null;
		}

	}

}
