package com.hcl.main;

import java.util.Scanner;

import com.hcl.model.Circle;
import com.hcl.model.Rectangle;
import com.hcl.model.Square;

public class ShapeMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Circle circle = new Circle();
		Square square = new Square(null);
		Rectangle rectangle = new Rectangle(null);
		try {

			System.out.println("circle");
			System.out.println("square");
			System.out.println("rectangle");
			System.out.println("Enter the shape:");

			String str = scanner.next();
			// if circle
			switch (str) {
			case "circle":
				System.out.println("enter the radius:");
				float rad = scanner.nextFloat();
				System.out.println("area of circle is:" + circle.calculateArea(rad));
				break;
			case "square":
				System.out.println("enter side:");
				int sid = scanner.nextInt();
				System.out.println("area os square" + square.calculateArea(sid));
				break;
			case "rectangle":
				System.out.println("enter length and breadth");
				int len = scanner.nextInt();
				int bre = scanner.nextInt();
				System.out.println("area of rectangle:" + rectangle.calculateArea(len, bre));
				break;

			default:
				System.err.println("Enter valid option which is case sensitve");
				break;
			}
		} finally {
			scanner = null;
			circle = null;
			square = null;
			rectangle = null;

		}

	}
}
