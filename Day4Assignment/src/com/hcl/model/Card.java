/*3.Create an abstract class named Card with the following protected attributes / member variables.
String holderName;
String cardNumber;
String expiryDate;
Include appropriate getters and setters.
Include appropriate constructors. In the 3-argument constructor, the order of the arguments is holderName, cardNumber, expiryDate.*/
package com.hcl.model;

public abstract class Card {
	protected String holderName;
	protected String cardNumber;
	protected String expiryDate;

	public Card() {
		super();
	}

	public Card(String holderName, String cardNumber, String expiryDate) {
		super();
		this.holderName = holderName;
		this.cardNumber = cardNumber;
		this.expiryDate = expiryDate;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

}
