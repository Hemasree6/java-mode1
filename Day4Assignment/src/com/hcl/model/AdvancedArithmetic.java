//2.You are given an interface AdvancedArithmetic which contains a method signature int divisor_sum(int n). You need to write a class called MyCalculator which implements the interface.
package com.hcl.model;

public interface AdvancedArithmetic {
	int divisorSum(int n);

}
