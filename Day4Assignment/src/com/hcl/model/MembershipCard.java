package com.hcl.model;

public class MembershipCard extends Card {

	private int rating;

	public MembershipCard(int rating) {
		super();
		this.rating = rating;
	}

	public MembershipCard() {
		super();

	}

	public MembershipCard(String holderName, String cardNumber, String expiryDate, int rating) {
		super(holderName, cardNumber, expiryDate);

	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

}
