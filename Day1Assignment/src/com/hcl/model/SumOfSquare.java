//9. Write a program to read a number, calculate the sum of squares of even digits (values) present in the given number.
package com.hcl.model;

public class SumOfSquare {
	public static int evenDigit(int number) {
		int val;
		int sum = 0;
		while (number > 0) {
			val = number % 10;
			if ((val % 2) == 0)
				sum += val * val;
			number /= 10;

		}
		return sum;
	}

}
