package com.hcl.main;

import com.hcl.model.SumOfOdd;

public class MainSumOfOdd {
	public static void main(String[] args) {
		SumOfOdd odd = new SumOfOdd();
		int value = odd.CheckOdd(84228);
		if (value == -1) {
			System.out.println("Sum of odd digits is even");
		} else {
			System.out.println("Sum of odd digits is odd");
		}
	}
}
