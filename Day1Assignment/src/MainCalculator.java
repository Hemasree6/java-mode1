
public class MainCalculator {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();

		System.out.println("sum of numbers : " + calculator.findSum(20, 4));
		System.out.println("Difference of numbers : " + calculator.findDifference(20, 4));
		System.out.println("Multiplication of numbers : " + calculator.findMultiplication(20, 4));
		System.out.println("Division of numbers : " + calculator.findDivision(20, 4));
		System.out.println("Remainder of numbers : " + calculator.findRemainder(20, 4));
	}

}
