//4.Write a Java program to swap two variables

public class Swap {

	public static void main(String[] args) {
		int num1 = 100, num2 = 200;
		System.out.println("Before swapping : num1, num2 = " + num1 + ", " + +num2);
		int temp = num1;
		num1 = num2;
		num2 = temp;
		System.out.println("After swaping:num1, num2 = " + num1 + ", " + +num2);

	}

}
