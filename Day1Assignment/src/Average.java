//3.Write a Java program that takes three numbers as input to calculate and print the average of the numbers.

public class Average {

	public static void main(String[] args) {

		int num1 = 10;
		int num2 = 20;
		int num3 = 30;
		int sum = num1 + num2 + num3;
		float average = sum / 3;
		System.out.println("Average of three numbers:" + average);

	}
}
