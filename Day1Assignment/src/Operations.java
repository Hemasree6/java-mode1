
//1.Write a Java program to print the result of the following operations.  

public class Operations {

	public static void main(String[] args) {
		int a = -5 + 8 * 6;
		float b = (55 + 9) % 9;
		float c = 20 + -3 * 5 / 8;
		float d = 5 + 15 / 3 * 2 - 8 % 3;

		System.out.println(a + "\n" + b + "\n" + c + "\n" + d);
	}

}
