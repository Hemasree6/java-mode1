package com.hcl.main;

import java.util.Scanner;
import com.hcl.model.SumOfNumber;

public class SumOfNumberMain {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		SumOfNumber numbers = new SumOfNumber();
		System.out.println("enter the number of elements:");
		int n = scanner.nextInt();
		System.out.println("enter the numbers:");
		int[] array = new int[5];
		for (int i = 0; i < n; i++) {
			array[i] = scanner.nextInt();
		}

		System.out.println("sum of given numbers:" + numbers.add(array));
		scanner.close();

	}

}
