package com.hcl.main;

import com.hcl.model.Room;

public class MainRoom {
	public static void main(String[] args) {
		Room room = new Room();
		room.setRoomNo(6);
		room.setRoomType("Guest Room");
		room.setRoomArea("Srikalahasti");
		room.setAcMachine("LG");
		System.out.println("Room number is " + room.getRoomNo());
		System.out.println("Room Type is " + room.getRoomType());
		System.out.println("Room Area is " + room.getRoomArea());
		System.out.println("ACMachine is " + room.getAcMachine());

	}
}
