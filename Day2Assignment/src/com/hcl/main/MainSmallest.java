package com.hcl.main;

import com.hcl.model.Smallest;

public class MainSmallest {
	public static void main(String[] args) {

		int small = Smallest.findSmallestNumber(10, 30, 60);
		System.out.println("The Smallest number is " + small);

	}
}
