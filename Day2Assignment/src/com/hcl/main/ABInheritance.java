package com.hcl.main;

import com.hcl.model.B;

public class ABInheritance {

	public static void main(String[] args) {
		B b = new B();
		b.setEmpno(9160);

		b.setEmpname("Kota Hemasree");

		System.out.println(b.getEmpno());
		System.out.println(b.getEmpname());
		b.setSalary(1000.30f);
		System.out.println(b.getSalary());
	}
}