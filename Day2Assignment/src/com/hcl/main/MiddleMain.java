package com.hcl.main;

import java.util.Scanner;
import com.hcl.model.Middle;

public class MiddleMain {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Input a string: ");
		String str = scanner.nextLine();
		System.out.print("The middle character in the string: " + Middle.middle(str) + "\n");
		scanner.close();
	}

}
