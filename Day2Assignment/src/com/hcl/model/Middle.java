/*2.Write a Java method to display the middle character of a string. 
 a) If the length of the string is odd there will be two middle characters. 
 b) If the length of the string is even there will be one middle character. 
  Input a string: 367  
 Expected Output:                                                                     
The middle character in the string: 6 
a) If the length of the string is odd there will be two middle characters.
b) If the length of the string is even there will be one middle character.*/
package com.hcl.model;

public class Middle {

	public static String middle(String str) {
		int position;
		int length;
		String result;
		if (str.length() % 2 == 0) {
			position = str.length() / 2 - 1;
			length = 2;
		} else {
			position = str.length() / 2;
			length = 1;
		}
		result = str.substring(position, position + length);
		return result;
	}
}
