/*3. Write a Java method to count all vowels in a string.
 * Input the string: Hcl Technologies  

 Expected Output: 

Number of  Vowels in the string: 5 */
package com.hcl.model;

public class Vowel {
	public static int CountVowel(String str) {
		int count = 0;
		for (int i = 0; i < str.length(); i++) {
			if (str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i' || str.charAt(i) == 'o'
					|| str.charAt(i) == 'u') {
				count++;
			}
		}
		return count;
	}
}
