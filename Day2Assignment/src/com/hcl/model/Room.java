// 4.Write a program to create a room class, the attributes of this class is roomno, roomtype, roomarea and ACmachine. In this class the member functions are setdata and displaydata.
package com.hcl.model;

public class Room {

	private int roomNo;
	private String roomType;
	private String roomArea;
	private String AcMachine;

	public Room() {
		super();
	}

	public Room(int roomNo, String roomType, String roomArea, String acMachine) {
		super();
		this.roomNo = roomNo;
		this.roomType = roomType;
		this.roomArea = roomArea;
		AcMachine = acMachine;
	}

	public int getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getRoomArea() {
		return roomArea;
	}

	public void setRoomArea(String roomArea) {
		this.roomArea = roomArea;
	}

	public String getAcMachine() {
		return AcMachine;
	}

	public void setAcMachine(String acMachine) {
		AcMachine = acMachine;
	}

}