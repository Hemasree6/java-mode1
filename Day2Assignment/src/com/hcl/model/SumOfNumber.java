/*6. Your task is to create the class Addition and the required methods so that the code prints  the sum of the numbers passed to the function addition. 
Note: Your addition method in the Addition class must print the sum as given in the Sample Output 
Sample Input 
1 
2          
3 
4 
5 
6
Sample Output 
1+2=3 
1+2+3=6 
1+2+3+4+5=15 
1+2+3+4+5+6=21 */

package com.hcl.model;

public class SumOfNumber {
	public int add(int[] numbers) {
		int sum = 0;
		for (int num : numbers) {
			if (sum != 0) {
				System.out.print("+");
			}
			sum += num;
			System.out.print(num);
		}
		System.out.println("=" + sum);
		return sum;
	}
}
