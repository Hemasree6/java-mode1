//1.Write a Java method to find the smallest number among three numbers.

package com.hcl.model;

public class Smallest {
	public static int findSmallestNumber(int num1, int num2, int num3) {
		int value;
		if (num1 < num2 && num1 < num3) {
			value = num1;
		} else if (num2 < num1 && num2 < num3) {
			value = num2;
		} else {
			value = num3;
		}
		return value;
	}
}
